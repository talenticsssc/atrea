import Route from '@ember/routing/route';
import { inject as service } from "@ember/service"


export default Route.extend({
	session: service(),
	currentUser: service(),
	beforeModel(){
		return this.get('session').fetch().then(()=>{

            if(this.get('session.currentUser.uid')){

            	return this.get('currentUser.account').then((account)=>{


                	return this.transitionTo(account.get('perfil'))
            	})
            	
                
            }
        }).catch(()=>{
        	if(this.get('session.currentUser.uid')){
            	return this.get('currentUser.account').then((account)=>{
            		transition.abort()
                    console.log(account.get('perfil'))
                	return this.transitionTo(account.get('perfil'))
            	})
            	
                
            }
        })
	}
});
