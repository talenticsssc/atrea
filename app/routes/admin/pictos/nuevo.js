import Route from '@ember/routing/route';

export default Route.extend({
	 queryParams: {
    custom: {
      refreshModel: true
    }
  },
	model(params){
		console.log(params.custom)
		return this.store.createRecord('picto', {
			isCustom: params.custom
		})
	}
});
