import Route from '@ember/routing/route';
import {inject as service} from "@ember/service"
import {computed} from "@ember/object";


export default Route.extend({
	currentUser: service(),
	model(){
		
		return this.get('currentUser.account').then((account)=>{
			return this.store.createRecord('stage', {
			institucion: account.get('institution')
		})
				
		})
	}


});
