import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import AuthenticatedRoute from '../mixins/authenticated-route';

export default Route.extend(AuthenticatedRoute, {
	currentUser: service(),
	beforeModel(transition){
		this._super(...arguments)
		return this.get('session').fetch().then(()=>{
			return this.get('currentUser.account').then((account)=>{
				if(account.get('perfil')!='superadministrador'){
					transition.abort()
					return this.transitionTo(account.get('perfil'))
				}
			})
		}).catch((error)=>{
			return this.get('currentUser.account').then((account)=>{
				if(account.get('perfil')!='superadministrador'){
					transition.abort()
					return this.transitionTo(account.get('perfil'))
				}
			})
		})
	},

	model(){
		return {
			nombre: "Roberto",
			apellidos: "Yoc"
		}
	}
});
