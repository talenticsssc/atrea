import Controller from '@ember/controller';

export default Controller.extend({
	actions:{
	delete(stage)
	{
			swal({
		  title: 'Estas seguro?',
		  text: "No podrás revertir los cambios",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Si, borrar'
		  }).then((result) => {
		  if (result.value) {
		    stage.destroyRecord().then(()=>{
		    swal(
		      'Eliminada!',
		      'La etapa ha sido eliminada con Exito',
		      'success'
		        )
		       })
		     }
		    })
		 }
  }
});
