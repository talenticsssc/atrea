import Controller from '@ember/controller';

import {computed} from "@ember/object";

export default Controller.extend({
	categories: computed(function(){
		return this.store.findAll('category')
	}),
	

	actions: {	
		guardar(stage){
			stage.save().then(()=>{
				window.swal(
					'Guardado',
					'La etapa se ha guardado exitosamente',
					'success'
				)
				this.transitionToRoute("admin.etapas")
			})
		}
}

});
