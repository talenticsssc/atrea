import Controller from '@ember/controller';

export default Controller.extend({
		actions: { 
		edit(stage){
			stage.save().then(()=>{
					window.swal(
						'Guardado',
						'La institución se ha editado exitosamente',
						'success'
					)
					this.transitionToRoute("admin.etapas")

			})
		}
	}
});


