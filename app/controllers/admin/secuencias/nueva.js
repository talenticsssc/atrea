import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
	actions:{
		firebase: service('firebaseApp'),

		guardar(sequence){
			sequence.save().then(()=>{
     		//mandar notificacion de copletado
     				window.swal(
                    'Guardado!',
                    'La secuencia ha sido creada',
                    'success')      
                    this.transitionToRoute("admin.secuencias");

     			})
		
		}
	}
});
