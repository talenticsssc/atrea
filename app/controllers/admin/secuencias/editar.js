import Controller from '@ember/controller';

export default Controller.extend({
	actions:{
	edit(secuencia){
			secuencia.save().then(()=>{
					window.swal(
						'Guardado',
						'La secuencia se ha editado exitosamente',
						'success'
					)
					this.transitionToRoute("admin.secuencias")

			})
		}
	}

});
