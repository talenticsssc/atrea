import Controller from '@ember/controller';

export default Controller.extend({
	actions: { 
		edit(achievement){
			achievement.save().then(()=>{
					window.swal(
						'Guardado',
						'El logro se ha editado exitosamente',
						'success'
					)
					this.transitionToRoute("admin.logros")

			})
		}
	}
});
