import Controller from '@ember/controller';


export default Controller.extend({

	actions: {
		guardar(achievement){
			achievement.save().then(()=>{
            	 
              window.swal(
              'Guardado!',
              'El logro ha sido registrado',
              'success')      
              this.transitionToRoute("admin.logros");
		    })
		}
  	}
});
