import Controller from '@ember/controller';

export default Controller.extend({
	actions: {
		delete(logro)
		{
			swal({
			  title: 'Estas seguro?',
			  text: "No podrás revertir los cambios",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Si, borrar'
			}).then((result) => {
				if (result.value) {
					logro.destroyRecord().then(()=>{
						swal(
							'Eliminado',
							'El logro ha sido eliminado con exito',
							'success')
					})
				}
			})
		}
	}
});
