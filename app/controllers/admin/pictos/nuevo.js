import Controller from '@ember/controller';
import {computed} from "@ember/object";
export default Controller.extend({
	

  categories: computed(function(){
		return this.store.findAll('category')
	}),
  actions: {
    foo() { },

    guardar(picto){
			picto.save().then(()=>{
				window.swal(
					'Guardado',
					'La institución se ha guardado exitosamente',
					'success'
				)
				this.transitionToRoute("admin.pictos.custom")
			})
		}
  }
});
