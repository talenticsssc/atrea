import Controller from '@ember/controller';

import {computed} from "@ember/object";
export default Controller.extend({
	

  	categories: computed(function(){
		return this.store.findAll('category')
	}),
	actions: { 
		edit(picto){
			picto.save().then(()=>{
					window.swal(
						'Actualizado',
						'La institución se ha editado exitosamente',
						'success'
					)
					this.transitionToRoute("admin.pictos.custom")

			})
		}
	}
});
