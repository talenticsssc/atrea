import Controller from '@ember/controller';

export default Controller.extend({
	actions:{
	delete(picto)
	{
		swal({
  title: 'Estas seguro?',
  text: "No podrás revertir los cambios",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, borrar'
  }).then((result) => {
  if (result.value) {
    picto.destroyRecord().then(()=>{
    swal(
      'Eliminada!',
      'La institucion ha sido eliminada con Exito',
      'success'
        )
       })
     }
    })
	 }
  }
});
