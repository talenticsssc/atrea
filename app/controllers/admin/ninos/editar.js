import Controller from '@ember/controller';

export default Controller.extend({
	actions: { 
		edit(usuario){
			usuario.save().then(()=>{
					window.swal(
						'Guardado',
						'El usuario se ha editado exitosamente',
						'success'
					)
					this.transitionToRoute("admin.ninos")

			})
		}
	}
});
