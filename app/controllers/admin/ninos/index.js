import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
	  firebase: service('firebaseApp'),
	  actions:{
	delete(usuario){
		swal({
		  title: 'Estas seguro?',
		  text: "No podrás revertir los cambios",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Si, borrar'
		}).then((result) => {
		  if (result.value) {
		  	usuario.get('account').then((account)=>{
		  		//TODO: Borrar correo de auth
		  		let user = this.get("firebase").auth().currentUser;
				user.delete().then(function() {
					account.destroyRecord().then(()=>{
			  			usuario.destroyRecord().then(()=>{
						    swal(
						      'Eliminado!',
						      'El usuario ha sido eliminado con Exito',
						      'success'
				    			)
						})
			  		})
				  // User deleted.
				}).catch(function(error) {
				  // An error happened.
				});
		  	    })		    
  	}
    })
	}
}
});
