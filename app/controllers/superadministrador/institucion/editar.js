import Controller from '@ember/controller';

import {computed} from "@ember/object";



export default Controller.extend({
	accounts: computed(function(){
		return this.store.findAll('account')
	}),
	administradores: computed('accounts.@each.perfil', function(){

		//this.get('accounts').filterBy('perfil,"admin"')
		let array =[]
		this.get('accounts').forEach((account)=>{
			if(account.get('perfil')=="admin")
				array.push(account)			
		})
		return array
	}),

	actions: {	
		editar(institution){
			institution.save().then(()=>{
				window.swal(
					'Guardado',
					'La institución se ha guardado exitosamente',
					'success'
				)
				this.transitionToRoute("superadministrador.instituciones")
			})
		}

	}
});
