import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import {computed} from "@ember/object";
export default Controller.extend({


	firebase: service('firebaseApp'),
	
  institutions: computed(function(){
    return this.store.findAll('institution')
  }),
  actions: {
    foo() { },
		guardar(){
          let newemail = this.get("user") + "@atrea.mx";
          let nombre = this.get("nombre");
          let apellido = this.get("apellido");
          let pass = this.get("pass");
          let institution = this.get("institution");
          this.get('firebase').auth().createUserWithEmailAndPassword(newemail, pass).then((usuario)=>{
          	 
            this.get('store').createRecord('account', {
              uid: usuario.uid,
              nombre: nombre,
              apellido: apellido,
              perfil: "admin",
              institution: institution
            }).save().then(()=>{
            	 
              window.swal(
              'Guardado!',
              'El usuario ha sido registrado',
              'success')      
              this.transitionToRoute("superadministrador.usuarios");
		        })
		      })
	  }
  }
});

