import Controller from '@ember/controller';


export default Controller.extend({
   	actions: {
		delete(usuario){
			swal({
			  title: 'Estas seguro?',
			  text: "No podrás revertir los cambios",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Si, borrar'
			}).then((result) => {
			        if (result.value){
			        	usuario.destroyRecord().then(()=>{
			        		swal(
						      'Eliminada!',
						      'El usuario ha sido eliminada con Exito',
						      'success'
						        )
			        	})
			        }
					})    	
  		}
 	}
});









