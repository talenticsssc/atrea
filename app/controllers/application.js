import Controller from '@ember/controller';

export default Controller.extend({
	session: Ember.inject.service(),
  beforeModel: function() {
    return this.get('session').fetch().catch(function() {});
  },
  actions: {
    signOut: function() {
      this.get('session').close();
      this.transitionToRoute("login");
    }
  }
});

