import DS from 'ember-data';

export default DS.Model.extend({
	url: DS.attr('string'),
	width: DS.attr('number'),
	name: DS.attr('string'),
	male: DS.attr('boolean'),
	female: DS.attr('boolean'),
	picto: DS.belongsTo('picto'),
});
