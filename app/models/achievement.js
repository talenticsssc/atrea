import DS from 'ember-data';

export default DS.Model.extend({
	institution: DS.belongsTo('institution'),
	name: DS.attr('string'),
	description: DS.attr('string')
});
