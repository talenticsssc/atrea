import DS from 'ember-data';

export default DS.Model.extend({
	url: DS.attr('string'),
	width: DS.attr('number'),
	name: DS.attr('string'),
	date: DS.attr('date'),
	institution: DS.belongsTo('institution'),
});
