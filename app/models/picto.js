import DS from 'ember-data';

export default DS.Model.extend({
	name: DS.attr('string'),
	voiceclips: DS.hasMany('voiceclip'),
	images: DS.hasMany('image'),
	kid: DS.belongsTo('kid'),
	sequence: DS.belongsTo('sequence'),
	category: DS.belongsTo('category'),
	isCustom: DS.attr('boolean')
});
