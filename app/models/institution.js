import DS from 'ember-data';

export default DS.Model.extend({
	name:DS.attr('string'),

	admins:DS.hasMany('account'),
	adminMaster: DS.belongsTo("account", {inverse: "institution"}),
	kids:DS.hasMany('kid'),
	checkpoints:DS.hasMany('checkpoint'),
	sequences:DS.hasMany('sequence'),
});
