import DS from 'ember-data';

export default DS.Model.extend({
	secuencias:DS.hasMany("sequence"), 
	pictos:DS.hasMany("picto"),
	account:DS.belongsTo("account"),
    instituciones:DS.belongsTo("institution"),
	fnacimiento:DS.attr("date"),
	nombre: DS.attr('string'),
	direccion: DS.attr('string'),
	telefono: DS.attr('string'),
	apellido: DS.attr('string'),
	tutor:DS.attr('string'),


});
