import DS from 'ember-data';

export default DS.Model.extend({
	name:DS.attr('string'),
	category:DS.hasMany('categorys'),
	institucion:DS.belongsTo('institution')

});
