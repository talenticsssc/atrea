import DS from 'ember-data';

export default DS.Model.extend({
	url: DS.attr('string'),
	width: DS.attr('number'),
	name: DS.attr('string'),
	pictos: DS.hasMany('picto'),
	stage: DS.belongsTo('stage'),
});
