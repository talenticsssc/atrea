import DS from 'ember-data';

export default DS.Model.extend({
	url: DS.attr('string'),
	institution: DS.belongsTo('institution'),
	account: DS.belongsTo('account')
});
