import DS from 'ember-data';

export default DS.Model.extend({
	top:DS.attr('number'),
	bottom:DS.attr('number'),
	result:DS.attr('number'),
	isverified:DS.attr('boolean')
});
