import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('admin', function() {
    this.route('logros', function() {
      this.route('nuevo');
      this.route('editar',{path:"/editar/:achievement_id"});

    });
    this.route('pictos', function() {
      this.route('standard');
      this.route('custom');
      this.route('nuevo');
      this.route('editar',{path:"/editar/:picto_id"});
    });
    this.route('secuencias', function() {
      this.route('nueva');
      this.route('editar',{path:"editar/:sequence_id"});
    });
    this.route('etapas', function() {
      this.route('nueva');
      this.route('editar',{path:"editar/:stage_id"});
    });
    this.route('ninos', function() {
      this.route('nuevo');
      this.route('editar',{path:"editar/:kid_id"});
    });
  });
  this.route('nino', {path: "/app"},function() {
    this.route('librero');
    this.route('pictos', function() {
      this.route('ver',{path:"/editar/:picto_id"});
    });
    this.route('secuencias', function() {
      this.route('ver',{path: "/:sequence_id"});
    });    
    this.route('config', function() {
      this.route('pictos', function() {
        this.route('nuevo');
        this.route('editar',{path: "/editar/:picto_id"});
      });
      this.route('secuencias', function() {
        this.route('nuevo');
        this.route('editar',{path:"editar/:sequence_id"});
      });
    });
  });

  this.route('superadministrador', function() {
    this.route('usuarios');
    this.route('instituciones');
    this.route('institucion', function() {
      this.route('nueva');
      this.route('editar', {path:"editar/:institution_id"});
    });
    this.route('usuario', function() {
      this.route('nuevo');
      this.route('editar', {path:"editar/:account_id"});
    });

  });
  this.route('login');
  this.route('dir');
  this.route('register');
});

export default Router;
