import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | superadministrador/usuario/editar', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let controller = this.owner.lookup('controller:superadministrador/usuario/editar');
    assert.ok(controller);
  });
});
