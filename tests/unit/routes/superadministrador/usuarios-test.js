import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | superadministrador/usuarios', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:superadministrador/usuarios');
    assert.ok(route);
  });
});
