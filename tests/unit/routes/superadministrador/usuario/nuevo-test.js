import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | superadministrador/usuario/nuevo', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:superadministrador/usuario/nuevo');
    assert.ok(route);
  });
});
