import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | nino/pictos/ver', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:nino/pictos/ver');
    assert.ok(route);
  });
});
