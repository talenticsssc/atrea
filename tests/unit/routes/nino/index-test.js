import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | nino/index', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:nino/index');
    assert.ok(route);
  });
});
